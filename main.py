from flask import Flask, render_template, request, redirect, url_for, session,jsonify
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re

app = Flask(__name__)

# Change this to your secret key (can be anything, it's for extra protection)
app.secret_key = 'your secret key'

# Enter your database connection details below
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'Hello@1234'
app.config['MYSQL_DB'] = 'pythonlogin'

# Intialize MySQL
mysql = MySQL(app)


@app.route('/pythonlogin/', methods=['GET', 'POST'])
def login():
    # Output message if something goes wrong...
    msg = ''
    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts3 WHERE username = %s AND password = %s', (username, password,))
        # Fetch one record and return result
        account = cursor.fetchone()
        # If account exists in accounts table in out database
        if account:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            # session['id'] = account['id']
            session['username'] = account['username']
            # Redirect to home page
            return redirect(url_for('home'))
        else:
            # Account doesnt exist or username/password incorrect
            msg = 'Incorrect username/password!'
    # Show the login form with message (if any)
    return render_template('index.html', msg=msg)

# http://localhost:41005/python/logout - this will be the logout page
@app.route('/pythonlogin/logout')
def logout():
    # Remove session data, this will log the user out
    session.pop('loggedin', None)
    # session.pop('id', None)
    session.pop('username', None)
    # Redirect to login page
    return redirect(url_for('login'))

# http://localhost:41005/pythinlogin/register - this will be the registration page, we need to use both GET and POST requests
@app.route('/pythonlogin/register', methods=['GET', 'POST'])
def register():
    # Output message if something goes wrong...
    msg = ''
    # Check if "username", "password" and "email" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        firstname = request.form['firstname']
        lastname= request.form['lastname']
        companyname = request.form['companyname']
            # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts3 WHERE username = %s', (username,))
        account = cursor.fetchone()
        # If account exists show error and validation checks
        if account:
            msg = 'Account already exists!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address!'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'Username must contain only characters and numbers!'
        elif not username or not password or not email:
            msg = 'Please fill out the form!'
        else:
            # Account doesnt exists and the form data is valid, now insert new account into accounts table
            cursor.execute('INSERT INTO accounts3 VALUES (%s, %s, %s,%s,%s,%s)', (username, password, email,firstname,lastname,companyname))
            mysql.connection.commit()
            msg = 'You have successfully registered!'
    elif request.method == 'POST':
        # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    # Show registration form with message (if any)
    return render_template('register.html', msg=msg)


# http://localhost:41005/pythinlogin/home - this will be the home page, only accessible for loggedin users
@app.route('/pythonlogin/home')
def home():
    # Check if user is loggedin
    if 'loggedin' in session:
        # User is loggedin show them the home page
        return render_template('home.html', username=session['username'])
    # User is not loggedin redirect to login page
    return redirect(url_for('login'))

# http://localhost:41005/pythinlogin/profile - this will be the profile page, only accessible for loggedin users
@app.route('/pythonlogin/profile')
def profile():
    # Check if user is loggedin
    if 'loggedin' in session:
        # We need all the account info for the user so we can display it on the profile page
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts3 WHERE username = %s', (session['username'],))
        account = cursor.fetchone()
        # Show the profile page with account info
        return render_template('profile.html', account=account)
    # User is not loggedin redirect to login page
    return redirect(url_for('login'))

@app.route('/pythonlogin/create',methods = ["GET","POST"])
def create():
    msg=''
    if request.method == 'POST' and 'title' in request.form and 'description' in request.form :
    #     # Create variables for easy access
        
        title = request.form['title']
        description = request.form['description']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        
        cursor.execute('SELECT * FROM post1 WHERE title = %s', (title,))
        account=cursor.fetchone
        if account:
            msg="exists"
        cursor.execute('INSERT INTO post1 VALUES (1,%s, %s)', (title, description,))
        mysql.connection.commit()
        msg="successfully posted"
    elif request.method == 'POST':
        msg="post something"
    return render_template('create.html',msg=msg)
        

@app.route('/pythonlogin/delete', methods=["DELETE"])
def delete():
    # x = request.form["x"]
    # y = request.form["y"]
    sql="DELETE FROM post1 WHERE title = 'dude'"
    cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute(sql)
    mysql.connection.commit()
    return "deleted successfully"

@app.route('/pythonlogin/update',methods=["POST"])
def update():
    # x = request.form["x"]
    # old = request.form["y"]
    # new = request.form["y1"]
    sql = "UPDATE post1 SET title = 'chaitu' WHERE description = 'say'"
    cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute(sql)
    mysql.connection.commit()
    return "updated successfully"

# @app.route("/pythonlogin/doprofile/<id>")
# def doprofile(id):
#     cur=mysql.connection.cursor()
#     cur.execute("select * from profile where id=%s", (id))
#     data=cur.fetchone()
#     if data:
#         session['logged_in']=True
#         return render_template('profile.html')
#     return render_template('feed.html')

@app.route('/pythonlogin/doprofile/<id>')
#this function gives the description of user
def doprofile(id):
    if 'loggedin' in session:
        cur=mysql.connection.cursor()
        cur.execute("select * from profile where id=%s",(id))
        data=dict(cur.fetchone())
        return data
    else:
        return {'message':"Login First"},401


@app.route('/pythonlogin/dologin/<string:UserName>/<string:password>')#using methods in flask
def dologin(UserName,password):
  cur=mysql.connection.cursor()
  cur.execute("select * from accounts3 where UserName=%s and password=%s",(UserName,password))
  data=cur.fetchone()
  if data:
    session['logged_in']=True
    return render_template('home.html')
  else:
    return render_template("register.html")

@app.route('/pythonlogin/querylogin')
def new_login_query():
	username=request.args['username']
	password=request.args['password']
	cursor = mysql.connection.cursor() 
	cursor.execute(f'SELECT * FROM accounts3 WHERE username = "{username}" AND password = "{password}"')
	account = cursor.fetchone()
	if account:                                         
		session['logged_in'] = True
		return render_template('home.html')
	return render_template('register.html')


@app.route('/pythonlogin/pathregister/<string:username>/<string:password>/<string:email>/<string:firstname>/<string:lastname>/<string:companyname>')
def pathregister(username,password,email,firstname,lastname,companyname):
# this function is used to create a new profile and is used to check if an account already
	cursor = mysql.connection.cursor()
	cursor.execute(f'SELECT * FROM accounts3 WHERE username ="{username}"')
	account = cursor.fetchone()
	if account:
		data = {
		"msg" : 'Account already exists !',
		}
		return jsonify(data)
	else:
		cursor.execute('INSERT INTO accounts3 VALUES (%s, %s, %s, %s, %s,%s)', (username, password, email, firstname,lastname, companyname))
		mysql.connection.commit()
		data = {
		"msg" : 'You have successfully registered !',
        
		"username": username,
        "password" : password,
		"email" : email,		
		"firstname": firstname,
		"lastname":lastname,
        "companyname":companyname,
		"status": "active"
		}
		return jsonify(data)

@app.route('/pythonlogin/queryregister')
def queryregister():
# this function is used to create a new profile and is used to check if an account already
    username=request.args['username']
    password=request.args['password']
    email=request.args['email']
    firstname=request.args['firstname']
    lastname=request.args['lastname']
    companyname=request.args['companyname']
    
    cursor = mysql.connection.cursor()
    cursor.execute(f'SELECT * FROM accounts3 WHERE username ="{username}"')
    account = cursor.fetchone()
    if account:
        data = {
        "msg" : 'Account already exists !',
        }
        return jsonify(data)
    
    cursor.execute('INSERT INTO accounts3 VALUES (%s, %s, %s, %s, %s,%s)', (username, password,email,firstname, lastname,companyname))
    mysql.connection.commit()
    data = {
    "msg" : 'You have successfully registered !',
    "username": username,
    "password" : password,
    "email" : email,
    "firstname": firstname,
    "lastname":lastname,
    "companyname":companyname
        }
    return jsonify(data)

@app.route('/pythonlogin/pathcreate/<string:title>/<string:description>')
def pathcreate(title,description):
# this function is used to create a new profile and is used to check if an account already
	cursor = mysql.connection.cursor()
	cursor.execute(f'SELECT * FROM post1 WHERE title ="{title}"')
	account = cursor.fetchone()
	if account:
		data = {
		"msg" : 'Account already exists !',
		}
		return jsonify(data)
	else:
		cursor.execute('INSERT INTO post1 VALUES (1,%s, %s)', (title,description))
		mysql.connection.commit()
		data = {
		"msg" : 'You have successfully registered !',
        
		"title":title,
        "description":description,
		"status": "active"
		}
		return jsonify(data)

@app.route('/pythonlogin/querycreate')
def querycreate():
# this function is used to create a new profile and is used to check if an account already
    title=request.args['title']
    description=request.args['description']
    
    cursor = mysql.connection.cursor()
    cursor.execute(f'SELECT * FROM post1 WHERE title ="{title}"')
    account = cursor.fetchone()
    if account:
        data = {
        "msg" : 'Account already exists !',
        }
        return jsonify(data)
    
    cursor.execute('INSERT INTO post1 VALUES (1,%s, %s)', (title,description))
    mysql.connection.commit()
    data = {
    "msg" : 'You have successfully registered !',
    "title": title,
    "description" : description
        }
    return jsonify(data)


@app.route('/pythonlogin/postmanregister',methods=["POST"])
def postmanregister():
# this function is used to create a new profile and is used to check if an account already
    data=request.json
    username=request.args['username']
    password=request.args['password']
    email=request.args['email']
    firstname=request.args['firstname']
    lastname=request.args['lastname']
    companyname=request.args['companyname']
    
    cursor = mysql.connection.cursor()
    cursor.execute(f'SELECT * FROM accounts3 WHERE username ="{username}"')
    account = cursor.fetchone()
    if account:
        data = {
        "msg" : 'Account already exists !',
        }
        return jsonify(data)
    else:
        cursor.execute('INSERT INTO accounts3 VALUES (%s,%s, %s, %s, %s, %s)', (username, password,email,firstname, lastname,companyname))
        mysql.connection.commit()
        data = {
        "username": username,
        "password" : password,
        "email" : email,
        "firstname": firstname,
        "lastname":lastname,
        "companyname":companyname,
        "status": "active"
                }
        return jsonify(data)


if __name__=='__main__':
    app.run(debug=True,port=41005)